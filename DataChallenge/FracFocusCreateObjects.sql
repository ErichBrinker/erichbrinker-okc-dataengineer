USE [FracFocusRegistry]
GO
/****** Object:  Table [dbo].[dimDate]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dimDate](
	[DateID] [int] IDENTITY(1,1) NOT NULL,
	[JobDateTime] [datetime] NULL,
	[JobDate] [date] NULL,
	[JobMonth] [nvarchar](30) NULL,
	[JobYear] [int] NULL,
 CONSTRAINT [PK_dimDate] PRIMARY KEY CLUSTERED 
(
	[DateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dimIngredient]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dimIngredient](
	[IngredientID] [int] IDENTITY(1,1) NOT NULL,
	[IngredientName] [varchar](150) NULL,
 CONSTRAINT [PK_dimIngredient] PRIMARY KEY CLUSTERED 
(
	[IngredientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dimStateCounty]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dimStateCounty](
	[StateCountyID] [int] IDENTITY(1,1) NOT NULL,
	[StateNumber] [varchar](2) NULL,
	[CountyNumber] [varchar](3) NULL,
	[StateName] [varchar](50) NULL,
	[CountyName] [varchar](50) NULL,
 CONSTRAINT [PK_dimStateCounty] PRIMARY KEY CLUSTERED 
(
	[StateCountyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dimSupplier]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dimSupplier](
	[SupplierID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [varchar](150) NULL,
 CONSTRAINT [PK_dimSupplier] PRIMARY KEY CLUSTERED 
(
	[SupplierID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dimType]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dimType](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_dimType] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dimWell]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dimWell](
	[WellID] [int] IDENTITY(1,1) NOT NULL,
	[APINumber] [varchar](14) NOT NULL,
	[WellName] [varchar](150) NOT NULL,
	[StateNumber] [varchar](2) NULL,
	[CountyNumber] [varchar](3) NULL,
	[StateName] [varchar](50) NULL,
	[CountyName] [varchar](50) NULL,
 CONSTRAINT [PK_dimWell] PRIMARY KEY CLUSTERED 
(
	[WellID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FactJobCount]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FactJobCount](
	[JobStartCount] [int] NULL,
	[dateID] [int] NOT NULL,
	[wellID] [int] NOT NULL,
	[supplierID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FactRecipePCT]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FactRecipePCT](
	[RecipeID] [int] IDENTITY(1,1) NOT NULL,
	[WellID] [int] NOT NULL,
	[PctIngredient] [float] NULL,
	[IngredientID] [int] NOT NULL,
	[PctTypeID] [int] NOT NULL,
 CONSTRAINT [PK_FactRecipePCT_1] PRIMARY KEY CLUSTERED 
(
	[RecipeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FactWaterVolume]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FactWaterVolume](
	[WellID] [int] NOT NULL,
	[StateCountyID] [int] NOT NULL,
	[TotalBaseWaterVolume] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[w27Base]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w27Base](
	[API_Number] [nvarchar](255) NULL,
	[Well_Name] [nvarchar](255) NULL,
	[Well_Status] [nvarchar](255) NULL,
	[Spud] [datetime] NULL,
	[First_Prod] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vwWellHeader]    Script Date: 8/1/2019 9:54:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwWellHeader] as
select  * from dimwell w join   dbo.w27Base b on substring(w.APINumber, 1, 10) = substring(b.API_Number, 1, 10) where statename = 'oklahoma'
GO
