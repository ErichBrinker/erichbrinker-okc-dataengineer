--drop table factJobCount
---Load Fact Table for Job Count
with s as (
Select distinct JobStartDate, APINumber, r.pkey, Supplier from RegistryUpload r inner join registryuploadpurpose p on r.pkey = p.pkeyregistryupload 
Where Supplier not like 'N/A%' and supplier not like 'Listed above%' and supplier not like 'Operator%' and len(supplier) > 1
)
Select count(JobStartDate) as JobStartCount, dd.dateID, dw.wellID, ds.supplierID from s into FactJobCount from s 
inner join DimSupplier ds on s.Supplier = ds.SupplierName
Inner join DimWell dw on s.APINumber = dw.APINumber
Inner join DimDate dd on s.JobStartDate = JobDateTime
--where pkey = 'D0ED12F6-7FA5-45AA-970F-000171948D99'
Group by jobstartdate, dd.DateID, dw.WellID, ds.SupplierID

--Load Fact with Min and Max Ingredients
--------
---Load the pct fact table with Max
With S as 
(
SELECT dw.wellID, max(PercentHFJob) as MaxPctIngredient--, MIN(PercentHFJob) as MinPctIngredient --into FactRecipePct

From FracFocusRegistry.dbo.RegistryUpload ru

Join FracFocusRegistry.[dbo].[RegistryUploadIngredients] ri
on ru.pkey=ri.pKeyDisclosure 

join DimWell dw on  dw.APINumber = ru.APINumber 
GROUP BY dw.wellID
)
, T as 
(
Select s.WellID, s.MaxPctIngredient, ri.IngredientName from S join dimWell dw on S.WelLID = dw.WellID
Join RegistryUpload RU on dw.APINumber = RU.APINumber
Join [dbo].[RegistryUploadIngredients] ri
on S.MaxPctIngredient = PercentHFJob and RU.pKey = ri.pKeyDisclosure
)
INSERT INTO dbo.FactRecipePCT(WellID, PctIngredient, IngredientID, PctTypeID)
Select distinct T.WellID, T.MaxPctIngredient as PctIngredient, di.IngredientID, 1 as PctTypeID from T inner join dimIngredient di on t.IngredientName = di.ingredientName where  len(isnull(T.IngredientName,''))> 0
---
--- load w/ Min
With S as 
(
SELECT dw.wellID, min(PercentHFJob) as MinPctIngredient--, MIN(PercentHFJob) as MinPctIngredient --into FactRecipePct

From FracFocusRegistry.dbo.RegistryUpload ru

Join FracFocusRegistry.[dbo].[RegistryUploadIngredients] ri
on ru.pkey=ri.pKeyDisclosure 

join DimWell dw on  dw.APINumber = ru.APINumber 
GROUP BY dw.wellID
)
, T as 
(
Select s.WellID, s.MinPctIngredient, ri.IngredientName from S join dimWell dw on S.WelLID = dw.WellID
Join RegistryUpload RU on dw.APINumber = RU.APINumber
Join [dbo].[RegistryUploadIngredients] ri
on S.MinPctIngredient = PercentHFJob and RU.pKey = ri.pKeyDisclosure
)
INSERT INTO dbo.FactRecipePCT(WellID, PctIngredient, IngredientID, PctTypeID)
Select distinct T.WellID, T.MinPctIngredient, di.IngredientID, 2 as PctTypeID from T inner join dimIngredient di on t.IngredientName = di.ingredientName where  len(isnull(T.IngredientName,''))> 0
---
------
--load fact watervolume
select dw.WellID, sc.StateCountyID, SUM(IsNull(TotalbaseWaterVolume, 0)) as TotalBaseWaterVolume into FactWaterVolume from dbo.RegistryUpload ru 
join dimWell dw on RU.APINumber = dw.APINumber
join dimstatecounty sc on ru.statenumber = sc.statenumber and ru.countynumber = sc.countynumber
where totalbasewatervolume is not null
group by WellID, statecountyid

