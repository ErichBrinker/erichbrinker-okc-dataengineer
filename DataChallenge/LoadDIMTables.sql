--Truncate table dbo.dimSupplier
INSERT INTO dbo.dimSupplier (SupplierName)
select distinct Supplier from dbo.registryuploadpurpose where supplier is not null and len(supplier) > 1

--- Fill DIM Well Table
SELECT distinct APINumber, WellName, StateNumber, CountyNumber, StateName, CountyName into dimwell
From FracFocusRegistry.dbo.RegistryUpload ru
Join FracFocusRegistry.[dbo].[RegistryUploadIngredients] ri
on ru.pkey=ri.pKeyDisclosure 
Left JOIN FracFocusRegistry.dbo.RegistryUploadPurpose rp
on ri.pKeyPurpose = rp.pKey

-- Fill DimDate Table
Select Distinct JobStartDate as JobDateTime, Cast(Jobstartdate as date) as JobDate, DateName(month, JobStartDate) as JobMonth,   Year(JobStartDate) as JobYear into 
dimDate
From FracFocusRegistry.dbo.RegistryUpload ru







---Load the pct fact table with Max
With S as 
(
SELECT dw.wellID, max(PercentHFJob) as MaxPctIngredient--, MIN(PercentHFJob) as MinPctIngredient --into FactRecipePct

From FracFocusRegistry.dbo.RegistryUpload ru

Join FracFocusRegistry.[dbo].[RegistryUploadIngredients] ri
on ru.pkey=ri.pKeyDisclosure 

join DimWell dw on  dw.APINumber = ru.APINumber 
GROUP BY dw.wellID
)
, T as 
(
Select s.WellID, s.MaxPctIngredient, ri.IngredientName from S join dimWell dw on S.WelLID = dw.WellID
Join RegistryUpload RU on dw.APINumber = RU.APINumber
Join [dbo].[RegistryUploadIngredients] ri
on S.MaxPctIngredient = PercentHFJob and RU.pKey = ri.pKeyDisclosure
)
INSERT INTO dbo.FactRecipePCT(WellID, PctIngredient, IngredientID, PctTypeID)
Select distinct T.WellID, T.MaxPctIngredient as PctIngredient, di.IngredientID, 1 as PctTypeID from T inner join dimIngredient di on t.IngredientName = di.ingredientName where  len(isnull(T.IngredientName,''))> 0
---
--- load w/ Min
With S as 
(
SELECT dw.wellID, min(PercentHFJob) as MinPctIngredient--, MIN(PercentHFJob) as MinPctIngredient --into FactRecipePct

From FracFocusRegistry.dbo.RegistryUpload ru

Join FracFocusRegistry.[dbo].[RegistryUploadIngredients] ri
on ru.pkey=ri.pKeyDisclosure 

join DimWell dw on  dw.APINumber = ru.APINumber 
GROUP BY dw.wellID
)
, T as 
(
Select s.WellID, s.MinPctIngredient, ri.IngredientName from S join dimWell dw on S.WelLID = dw.WellID
Join RegistryUpload RU on dw.APINumber = RU.APINumber
Join [dbo].[RegistryUploadIngredients] ri
on S.MinPctIngredient = PercentHFJob and RU.pKey = ri.pKeyDisclosure
)
INSERT INTO dbo.FactRecipePCT(WellID, PctIngredient, IngredientID, PctTypeID)
Select distinct T.WellID, T.MinPctIngredient, di.IngredientID, 2 as PctTypeID from T inner join dimIngredient di on t.IngredientName = di.ingredientName where  len(isnull(T.IngredientName,''))> 0
---
-- add ID column to the Fact Table
ALter Table dbo.factRecipePCT add RecipeID int identity(1,1)
Select * from dbo.factRecipePCT
-----------
-- load dimstatecounty
INSERT INTO dbo.dimStateCounty(StateNumber, CountyNumber, StateName, CountyName)
select distinct StateNumber, CountyNumber, stateName, countyname from registryupload

--load dimIngredient
INSERT INTO dbo.dimIngredient(IngredientName)
Select distinct(IngredientName) from dbo.RegistryUploadIngredients where ingredientName is not null order by IngredientName

--load dimType
INSERT INTO dbo.dimType(TypeName)
Values('Max')
INSERT INTO dbo.dimType(TypeName)
Values('Min')

--load fact watervolume
select dw.WellID, sc.StateCountyID, SUM(IsNull(TotalbaseWaterVolume, 0)) as TotalBaseWaterVolume into FactWaterVolume from dbo.RegistryUpload ru 
join dimWell dw on RU.APINumber = dw.APINumber
join dimstatecounty sc on ru.statenumber = sc.statenumber and ru.countynumber = sc.countynumber
where totalbasewatervolume is not null
group by WellID, statecountyid

